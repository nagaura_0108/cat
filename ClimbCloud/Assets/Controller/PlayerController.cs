using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rigid2D;
    Animator animator;
    public AudioClip SE1;
    AudioSource audioSource;
    
    float jumpForce = 340.0f;
    float walkForce =  2.0f;
    float maxWalkSpeed = 2.0f;
    void Start()
    {
        this.rigid2D = GetComponent<Rigidbody2D>();
        this.animator = GetComponent<Animator>();
        this.audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        //ジャンプ
        if(Input.GetKeyDown(KeyCode.Space) &&
            this.rigid2D.velocity.y == 0)
        {
            this.audioSource.PlayOneShot(SE1);
            //transform.right = (1,0,0) transform.up = (0,1,0) transform = (0,0,1)
            //-> (0,jumpForce,0)
            this.animator.SetTrigger("JumpTrigger");
            this.rigid2D.AddForce(transform.up * this.jumpForce);

        }

        int key = 0;
        //左右移動
        if(Input.GetKey(KeyCode.D))
        {
            key = 1;
        }
        if(Input.GetKey(KeyCode.A))
        {
            key = -1;
        }

        //プレイヤーの速度
        float speedx = Mathf.Abs(this.rigid2D.velocity.x);

        //スピード制限
        if(speedx < this.maxWalkSpeed)
        {
            this.rigid2D.AddForce(transform.right * key * this.walkForce);
        }

        //動く方向へ向く
        if(key != 0)
        {
            transform.localScale = new Vector3(key, 1, 1);
        }

        //画面外に出たがどうか
        if(transform.position.y < -10)
        {
            SceneManager.LoadScene("gameOverScene");
        }

        //プレイヤーの速度に応じてアニメーションの速度を変える
        if (this.rigid2D.velocity.y == 0)
        {
        this.animator.speed = speedx / 2.0f;
        }
        else
        {
            this.animator.speed = 1.0f;
        }
    }

    //ゴールに到着
    void OnTriggerEnter2D(Collider2D other)
    {
        SceneManager.LoadScene("ClearScene");
    }
}
