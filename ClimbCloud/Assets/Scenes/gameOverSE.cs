using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameOverSE : MonoBehaviour
{
    public AudioClip SE1,SE2;
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        //Coponent���擾
        this.audioSource = GetComponent<AudioSource>();
        this.audioSource.PlayOneShot(SE1);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            this.audioSource.PlayOneShot(SE2);
        }
    }
}
